#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <limits.h>

void printUserGroups(char* userName)
{
    int ngroups = NGROUPS_MAX;
    gid_t* groups = (gid_t*) malloc(ngroups * sizeof(gid_t));
    struct passwd* passwd = getpwnam(userName);
    
    if (getgrouplist(passwd->pw_name, passwd->pw_gid, groups, &ngroups) == -1)
    {
        fprintf(stderr, "Error: ngroups value too small, ngroups = %d\n", ngroups);
        exit(EXIT_FAILURE);
    }

    printf("| %s [ ", passwd->pw_name);
    for (int i = 0; i < ngroups; i++)
    {
        struct group* group = getgrgid(groups[i]);
        printf("%s ", group->gr_name);
    }
    printf("] ");
}

void printLoggedUsersId(char* userName)
{
    struct passwd* passwd = getpwnam(userName);
    printf("| %d %s ", passwd->pw_uid, passwd->pw_name);
}
