cc=gcc
soflags=-fPIC
cflags=-Wall -Wextra
main=who
lib=libwho.so.0.1
objects=*.o ${main} ${lib}

all : who libwho.so.0.1

${main} : who.c
	${cc} ${cflags} -o $@ $< -ldl
	
${lib} : libwho.o
	${cc} -shared ${soflags} -o $@ $< 
	
libwho.o : libwho.c
	${cc} ${cflags} -c ${soflags} $< 

clean : 
	rm -f ${objects}
