#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <utmpx.h>
#include <dlfcn.h>

void (*printUserGroups)(char*);
void (*printLoggedUsersId)(char*);

void printLoggedUsers(int checkId, int checkGroup)
{
	setutxent();
	struct utmpx* utmp;
	utmp = getutxent();
	while (utmp != NULL)
	{
		if (utmp->ut_type == USER_PROCESS)
		{
			printf("%s ", utmp->ut_user);

            if (checkId == 1 && printLoggedUsersId)
                printLoggedUsersId(utmp->ut_user);
            
            if (checkGroup == 1 && printUserGroups)
				printUserGroups(utmp->ut_user);

			printf("\n");
        }
        utmp = getutxent();
	}
	endutxent();
}

int main(int argc, char** argv)
{
	const static char* LIBWHO_SO = "./libwho.so.0.1";
	int checkId = 0;
	int checkGroup = 0;

	int option;
	while ((option = getopt(argc, argv, "ig")) != -1)
	{
		switch(option)
		{
			case 'i':
				checkId = 1;
				break;
			case 'g':
				checkGroup = 1;
				break;
			case '?':
                fprintf(stderr, "Usage: [-g] [-i]\n");
			default:
				exit(EXIT_FAILURE);
		}
	}

	void* handle = dlopen(LIBWHO_SO, RTLD_LAZY);
	if (!handle)
		fprintf(stderr, "%s\n", dlerror());
	else
	{
		printUserGroups = (void(*)(char*)) dlsym(handle, "printUserGroups");
		char* error = dlerror();
		if (error != NULL)
			fprintf(stderr, "%s\n", error);

		printLoggedUsersId = (void(*)(char*)) dlsym(handle, "printLoggedUsersId");
		error = dlerror();
		if (error != NULL)
			fprintf(stderr, "%s\n", error);
	}

	printLoggedUsers(checkId, checkGroup);
	
	if (handle)
		dlclose(handle);
		
	exit(EXIT_SUCCESS);
}
